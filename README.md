Role Name
=========

AutoLogic's Ansible Role for installing and managing Java installations on multiple UNIX platforms.

License
-------

BSD

Author Information
------------------

- Michael Crilly
- AutoLogic Technology Ltd
- http://www.mcrilly.me/
